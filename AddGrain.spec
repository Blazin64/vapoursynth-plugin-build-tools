%global _vpath_srcdir VapourSynth-AddGrain-r%{version}/

Name:           vapoursynth-addgrain
Version:        8
Release:        1%{?dist}
Summary:        AddGrain plugin for VapourSynth

License:        GPLv3
URL:            https://github.com/HomeOfVapourSynthEvolution/VapourSynth-AddGrain/
Source:         %{url}archive/r%{version}/VapourSynth-AddGrain-r%{version}.tar.gz

%if "%{_vendor}" == "debbuild"
%endif
%if "%{_vendor}" == "redhat"
BuildRequires:  meson
BuildRequires:  vapoursynth-devel
%endif

%description
%{summary}.

%prep
%autosetup -c

%build
%meson
%meson_build

%install
%meson_install

%check
%meson_test

%files
%{_libdir}/vapoursynth/libaddgrain.so
