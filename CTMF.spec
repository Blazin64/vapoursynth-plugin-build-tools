%global _vpath_srcdir VapourSynth-CTMF-r%{version}/

Name:           vapoursynth-ctmf
Version:        5
Release:        1%{?dist}
Summary:        Constant-time median filtering plugin for VapourSynth

License:        GPLv3
URL:            https://github.com/HomeOfVapourSynthEvolution/VapourSynth-CTMF/
Source:         %{url}archive/r%{version}/VapourSynth-CTMF-r%{version}.tar.gz

%if "%{_vendor}" == "debbuild"
%endif
%if "%{_vendor}" == "redhat"
BuildRequires:  meson
BuildRequires:  vapoursynth-devel
%endif

%description
%{summary}.

%prep
%autosetup -c

%build
%meson
%meson_build

%install
%meson_install

%check
%meson_test

%files
%{_libdir}/vapoursynth/libctmf.so
