%global _vpath_srcdir VapourSynth-DCTFilter-r%{version}/

Name:           vapoursynth-deblock
Version:        2.1
Release:        1%{?dist}
Summary:        DCTFilter plugin for VapourSynth

License:        MIT
URL:            https://github.com/HomeOfVapourSynthEvolution/VapourSynth-DCTFilter/
Source:         %{url}archive/r%{version}/VapourSynth-DCTFilter-r%{version}.tar.gz

%if "%{_vendor}" == "debbuild"
%endif
%if "%{_vendor}" == "redhat"
BuildRequires:  meson
BuildRequires:  vapoursynth-devel
BuildRequires:  fftw-devel
%endif

%description
%{summary}.

%prep
%autosetup -c

%build
%meson
%meson_build

%install
%meson_install

%check
%meson_test

%files
%{_libdir}/vapoursynth/libdctfilter.so
