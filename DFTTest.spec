%global _vpath_srcdir VapourSynth-DFTTest-r%{version}/

Name:           vapoursynth-dfttest
Version:        7
Release:        1%{?dist}
Summary:        DFTTest plugin for VapourSynth

License:        GPLv3
URL:            https://github.com/HomeOfVapourSynthEvolution/VapourSynth-DFTTest/
Source:         %{url}archive/r%{version}/VapourSynth-DFTTest-r%{version}.tar.gz

%if "%{_vendor}" == "debbuild"
%endif
%if "%{_vendor}" == "redhat"
BuildRequires:  meson
BuildRequires:  vapoursynth-devel
BuildRequires:  fftw-devel
%endif

%description
%{summary}.

%prep
%autosetup -c

%build
%meson
%meson_build

%install
%meson_install

%check
%meson_test

%files
%{_libdir}/vapoursynth/libdfttest.so
