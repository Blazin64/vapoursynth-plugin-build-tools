%global _vpath_srcdir VapourSynth-Deblock-r%{version}/

Name:           vapoursynth-deblock
Version:        6.1
Release:        1%{?dist}
Summary:        Deblock plugin for VapourSynth

License:        GPLv2
URL:            https://github.com/HomeOfVapourSynthEvolution/VapourSynth-Deblock/
Source:         %{url}archive/r%{version}/VapourSynth-Deblock-r%{version}.tar.gz

%if "%{_vendor}" == "debbuild"
%endif
%if "%{_vendor}" == "redhat"
BuildRequires:  meson
BuildRequires:  vapoursynth-devel
%endif

%description
%{summary}.

%prep
%autosetup -c

%build
%meson
%meson_build

%install
%meson_install

%check
%meson_test

%files
%{_libdir}/vapoursynth/libdeblock.so
