%global _vpath_srcdir VapourSynth-EEDI2-r%{version}/

Name:           vapoursynth-eedi2
Version:        7.1
Release:        1%{?dist}
Summary:        EEDI2 plugin for VapourSynth

License:        GPLv2
URL:            https://github.com/HomeOfVapourSynthEvolution/VapourSynth-EEDI2/
Source:         %{url}archive/r%{version}/VapourSynth-EEDI2-r%{version}.tar.gz

%if "%{_vendor}" == "debbuild"
%endif
%if "%{_vendor}" == "redhat"
BuildRequires:  meson
BuildRequires:  vapoursynth-devel
%endif

%description
%{summary}.

%prep
%autosetup -c

%build
%meson
%meson_build

%install
%meson_install

%check
%meson_test

%files
%{_libdir}/vapoursynth/libeedi2.so
