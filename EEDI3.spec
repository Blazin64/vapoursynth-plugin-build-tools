%global _vpath_srcdir VapourSynth-EEDI3-r%{version}/

Name:           vapoursynth-eedi3
Version:        4
Release:        1%{?dist}
Summary:        EEDI3 plugin for VapourSynth (OpenCL)

License:        GPLv2
URL:            https://github.com/HomeOfVapourSynthEvolution/VapourSynth-EEDI3/
Source:         %{url}archive/r%{version}/VapourSynth-EEDI3-r%{version}.tar.gz

Conflicts:      vapoursynth-eedi3cl

%if "%{_vendor}" == "debbuild"
%endif
%if "%{_vendor}" == "redhat"
BuildRequires:  meson
BuildRequires:  vapoursynth-devel
%endif

%description
%{summary}.

%prep
%autosetup -c

%build
%meson -Dopencl=false
%meson_build

%install
%meson_install

%check
%meson_test

%files
%{_libdir}/vapoursynth/libeedi3m.so
