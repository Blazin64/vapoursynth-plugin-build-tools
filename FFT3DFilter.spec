%global _vpath_srcdir VapourSynth-FFT3DFilter-64323f0fdee4dd4fe429ee6287906dbae8e7571c/

Name:           vapoursynth-fft3dfilter
Version:        64323f0
Release:        1%{?dist}
Summary:        FFT3DFilter plugin for VapourSynth

License:        GPLv2
URL:            https://github.com/myrsloik/VapourSynth-FFT3DFilter/
Source:         %{url}archive/64323f0fdee4dd4fe429ee6287906dbae8e7571c/VapourSynth-FFT3DFilter-64323f0fdee4dd4fe429ee6287906dbae8e7571c.tar.gz

%if "%{_vendor}" == "debbuild"
%endif
%if "%{_vendor}" == "redhat"
BuildRequires:  meson
BuildRequires:  vapoursynth-devel
BuildRequires:  fftw-devel
%endif

%description
%{summary}.

%prep
%autosetup -c

%build
%meson
%meson_build

%install
%meson_install

%check
%meson_test

%files
%{_libdir}/vapoursynth/libfft3dfilter.so
