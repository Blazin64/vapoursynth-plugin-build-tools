%global _vpath_srcdir vapoursynth-fluxsmooth-%{version}/

Name:           vapoursynth-fluxsmooth
Version:        2
Release:        1%{?dist}
Summary:        FluxSmooth plugin for VapourSynth

License:        Public Domain
URL:            https://github.com/dubhater/vapoursynth-fluxsmooth/
Source:         %{url}archive/v%{version}/vapoursynth-fluxsmooth-%{version}.tar.gz

%if "%{_vendor}" == "debbuild"
%endif
%if "%{_vendor}" == "redhat"
BuildRequires:  automake
BuildRequires:  libtool
BuildRequires:  vapoursynth-devel
%endif

%description
%{summary}.

%prep
%autosetup
./autogen.sh
./configure --libdir=%{_libdir}/vapoursynth CFLAGS="%{optflags}"

%build
make

%install
%make_install

%files
%{_libdir}/vapoursynth/libfluxsmooth.so
