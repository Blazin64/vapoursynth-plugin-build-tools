%global _vpath_srcdir KNLMeansCL-1ba48ce02d89c280537752234bcb0b14b7fac4f6/

Name:           vapoursynth-knlmeanscl
Version:        1ba48ce
Release:        1%{?dist}
Summary:        KNLMeansCL plugin for VapourSynth

License:        GPLv3
URL:            https://github.com/Khanattila/KNLMeansCL/
Source:         %{url}archive/1ba48ce02d89c280537752234bcb0b14b7fac4f6/KNLMeansCL-1ba48ce02d89c280537752234bcb0b14b7fac4f6.tar.gz

%if "%{_vendor}" == "debbuild"
%endif
%if "%{_vendor}" == "redhat"
BuildRequires:  meson
BuildRequires:  vapoursynth-devel
BuildRequires:  ocl-icd-devel
BuildRequires:  boost-devel
%endif

%description
%{summary}.

%prep
%autosetup -c

%build
%meson
%meson_build

%install
%meson_install

%check
%meson_test

%files
%{_libdir}/vapoursynth/libknlmeanscl.so
