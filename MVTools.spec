%global _vpath_srcdir vapoursynth-mvtools-%{version}/

Name:           vapoursynth-mvtools
Version:        23
Release:        1%{?dist}
Summary:        MVTools plugin for VapourSynth

License:        GPLv2
URL:            https://github.com/dubhater/vapoursynth-mvtools/
Source:         %{url}archive/%{version}/vapoursynth-mvtools-%{version}.tar.gz

%if "%{_vendor}" == "debbuild"
%endif
%if "%{_vendor}" == "redhat"
BuildRequires:  automake
BuildRequires:  vapoursynth-devel
BuildRequires:  fftw-devel
%endif

%description
%{summary}.

%prep
%autosetup -c

%build
%meson --libdir=%{_libdir}/vapoursynth
%meson_build

%install
%meson_install

%check
%meson_test

%files
%{_libdir}/vapoursynth/libmvtools.so
