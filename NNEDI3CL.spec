%global _vpath_srcdir VapourSynth-NNEDI3CL-r%{version}/

Name:           vapoursynth-nnedi3cl
Version:        8
Release:        1%{?dist}
Summary:        NNEDI3CL plugin for VapourSynth

License:        GPLv2
URL:            https://github.com/HomeOfVapourSynthEvolution/VapourSynth-NNEDI3CL/
Source:         %{url}archive/r${version}/VapourSynth-NNEDI3CL-r%{version}.tar.gz

%if "%{_vendor}" == "debbuild"
%endif
%if "%{_vendor}" == "redhat"
BuildRequires:  meson
BuildRequires:  vapoursynth-devel
BuildRequires:  ocl-icd-devel
BuildRequires:  boost-devel
%endif

%description
%{summary}.

%prep
%autosetup -c

%build
%meson
%meson_build

%install
%meson_install

%check
%meson_test

%files
%{_libdir}/vapoursynth/libnnedi3cl.so
%{_datadir}/NNEDI3CL/nnedi3_weights.bin
