# VapourSynth Plugin Build Tools

The scripts in this repository can be used to easily build RPM and DEB packages for a variety of VapourSynth plugins. (DEB building is still WIP.)

My current focus is building plugins for the older VapourSynth R51, which is the version currently available on Fedora 36. In the future, I intend to add spec files for building the latest versions of VapourSynth and the plugins.

At the moment, spec files are available for all plugins required by HAvsFunc r33. The only exceptions are SVP (an optional plugin) and the 4 required scripts.

### VapourSynth R51 Plugins

| Plugin      | Version | License       |
|-------------|---------|---------------|
| AddGrain    | r8      | GPLv3         |
| CTMF        | r5      | GPLv3         |
| DCTFilter   | r2\.1   | MIT           |
| Deblock     | R6\.1   | GPLv2         |
| DFTTest     | r7      | GPLv3         |
| EEDI2       | r7\.1   | GPLv2         |
| EEDI3       | r4      | GPLv2         |
| EEDI3CL\*   | r4      | GPLv2         |
| FFT3DFilter | 64323f0 | GPLv2         |
| FluxSmooth  | v2      | Public Domain |
| Fmtconv     | r22     | WTFPL         |
| HQDN3D      | eb820cb | GPLv2         |
| KNLMeansCL  | 1ba48ce | GPLv3         |
| MVTools     | v23     | GPLv2         |
| NNEDI3      | v12     | GPLv2         |
| NNEDI3CL    | r8      | GPLv2         |
| SangNom     | r42     | MIT           |
| TTempSmooth | r3\.1   | GPLv3         |
| ZNEDI3      | r2      | GPLv3         |

\* Technically, EEDI3CL and EEDI3 are the same plugin. They are just compiled using different settings. You can only install one or the other, not both!

### Dependencies

These packages must be installed to build the plugins.

Note that debbuild must be installed manually, since it is not available in any Debian/Ubuntu repositories that I know of.  If you go to the link in the table, an installable `.deb` file is available for download in the debbuild repository.

| Fedora            | Debian/Ubuntu                                      |
|-------------------|----------------------------------------------------|
| rpmdevtools       | \*[debbuild](https://github.com/debbuild/debbuild) |
| automake          | automake                                           |
| libatomic         | libatomic1                                         |
| libtool           | libtool                                            |
| meson             | meson                                              |
| yasm              | yasm                                               |
| boost-devel       | libbost-dev                                        |
| fftw-devel        | fftw-dev                                           |
| ocl-icd-devel     | ocl-icd-dev                                        |
| vapoursynth-devel | vapoursynth-dev                                    |

### Instructions (WIP)

**These instructions currently incomplete, so there are missing things like how to build DEB packages.**

1. Run the `download.sh` script to automatically download the source code archives required by the spec files. It will save the archives to `~/rpmbuild/SOURCES`.
2. Run the `build-rpms.sh` to automatically build RPMs for all currently supported plugins. Built RPMs should appear under `~/rpmbuild/RPMs`.
3. Install the RPMs with your preferred method, like DNF.
