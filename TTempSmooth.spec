%global _vpath_srcdir VapourSynth-TTempSmooth-r%{version}/

Name:           vapoursynth-ttempsmooth
Version:        3.1
Release:        1%{?dist}
Summary:        TTempSmooth plugin for VapourSynth

License:        GPLv3
URL:            https://github.com/HomeOfVapourSynthEvolution/VapourSynth-TTempSmooth/
Source:         %{url}archive/r%{version}/VapourSynth-TTempSmooth-r%{version}.tar.gz

%if "%{_vendor}" == "debbuild"
%endif
%if "%{_vendor}" == "redhat"
BuildRequires:  meson
BuildRequires:  vapoursynth-devel
%endif

%description
%{summary}.

%prep
%autosetup -c

%build
%meson
%meson_build

%install
%meson_install

%check
%meson_test

%files
%{_libdir}/vapoursynth/libttempsmooth.so
