#!/usr/bin/env bash

for SPEC in *.spec
do
    rpmbuild -bb "$SPEC"
done
