#!/usr/bin/env bash

echo "Downloading plugin source archives from GitHub."
echo "They will be saved to ~/rpmbuild/SOURCES"

sleep 3

mkdir -p ~/rpmbuild/SOURCES

wget -P ~/rpmbuild/SOURCES/ https://github.com/EleonoreMizo/fmtconv/archive/r22/fmtconv-r22.tar.gz \
https://github.com/Khanattila/KNLMeansCL/archive/1ba48ce02d89c280537752234bcb0b14b7fac4f6/KNLMeansCL-1ba48ce02d89c280537752234bcb0b14b7fac4f6.tar.gz \
https://github.com/HomeOfVapourSynthEvolution/VapourSynth-AddGrain/archive/r8/VapourSynth-AddGrain-r8.tar.gz \
https://github.com/HomeOfVapourSynthEvolution/VapourSynth-CTMF/archive/r5/VapourSynth-CTMF-r5.tar.gz \
https://github.com/HomeOfVapourSynthEvolution/VapourSynth-DCTFilter/archive/r2.1/VapourSynth-DCTFilter-r2.1.tar.gz \
https://github.com/HomeOfVapourSynthEvolution/VapourSynth-Deblock/archive/r6.1/VapourSynth-Deblock-r6.1.tar.gz \
https://github.com/HomeOfVapourSynthEvolution/VapourSynth-DFTTest/archive/r7/VapourSynth-DFTTest-r7.tar.gz \
https://github.com/HomeOfVapourSynthEvolution/VapourSynth-EEDI2/archive/r7.1/VapourSynth-EEDI2-r7.1.tar.gz \
https://github.com/HomeOfVapourSynthEvolution/VapourSynth-EEDI3/archive/r4/VapourSynth-EEDI3-r4.tar.gz \
https://github.com/myrsloik/VapourSynth-FFT3DFilter/archive/64323f0fdee4dd4fe429ee6287906dbae8e7571c/VapourSynth-FFT3DFilter-64323f0fdee4dd4fe429ee6287906dbae8e7571c.tar.gz \
https://github.com/dubhater/vapoursynth-fluxsmooth/archive/v2/vapoursynth-fluxsmooth-2.tar.gz \
https://github.com/Hinterwaeldlers/vapoursynth-hqdn3d/archive/eb820cb23f7dc47eb67ea95def8a09ab69251d30/vapoursynth-hqdn3d-eb820cb23f7dc47eb67ea95def8a09ab69251d30.tar.gz \
https://github.com/dubhater/vapoursynth-mvtools/archive/v23/vapoursynth-mvtools-23.tar.gz \
https://github.com/dubhater/vapoursynth-nnedi3/archive/v12/vapoursynth-nnedi3-12.tar.gz \
https://github.com/HomeOfVapourSynthEvolution/VapourSynth-NNEDI3CL/archive/r8/VapourSynth-NNEDI3CL-r8.tar.gz \
https://github.com/dubhater/vapoursynth-sangnom/archive/r42/vapoursynth-sangnom-r42.tar.gz \
https://github.com/HomeOfVapourSynthEvolution/VapourSynth-TTempSmooth/archive/r3.1/VapourSynth-TTempSmooth-r3.1.tar.gz \
https://github.com/sekrit-twc/vsxx/archive/3a89adc8ebd7ac756c0cf72fb45375ecc418df86/vsxx-3a89adc8ebd7ac756c0cf72fb45375ecc418df86.tar.gz \
https://github.com/sekrit-twc/znedi3/archive/r2/znedi3-r2.tar.gz
