%global _vpath_srcdir fmtconv-r%{version}/

Name:           vapoursynth-fmtconv
Version:        22
Release:        1%{?dist}
Summary:        Fmtconv plugin for VapourSynth

License:        WTFPL
URL:            https://github.com/EleonoreMizo/fmtconv/
Source:         %{url}archive/r%{version}/fmtconv-r%{version}.tar.gz

%if "%{_vendor}" == "debbuild"
%endif
%if "%{_vendor}" == "redhat"
BuildRequires:  automake
BuildRequires:  libatomic
BuildRequires:  libtool
BuildRequires:  vapoursynth-devel
%endif

%description
%{summary}.

%prep
%autosetup -c
echo lol
cd %{_builddir}/%{name}-%version/%{_vpath_srcdir}/build/unix
./autogen.sh
./configure --libdir=%{_libdir}/vapoursynth CPPFLAGS="%{optflags}"


%build
cd %{_builddir}/%{name}-%version/%{_vpath_srcdir}/build/unix
make

%install
cd %{_builddir}/%{name}-%version/%{_vpath_srcdir}/build/unix
%make_install

%files
%{_libdir}/vapoursynth/libfmtconv.so
