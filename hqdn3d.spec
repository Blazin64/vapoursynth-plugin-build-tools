%global _vpath_srcdir vapoursynth-hqdn3d-eb820cb23f7dc47eb67ea95def8a09ab69251d30/

Name:           vapoursynth-hqdn3d
Version:        eb820cb
Release:        1%{?dist}
Summary:        HQDN3D plugin for VapourSynth

License:        GPLv2
URL:            https://github.com/Hinterwaeldlers/vapoursynth-hqdn3d/
Source:         %{url}archive/eb820cb23f7dc47eb67ea95def8a09ab69251d30/vapoursynth-hqdn3d-eb820cb23f7dc47eb67ea95def8a09ab69251d30.tar.gz

%if "%{_vendor}" == "debbuild"
%endif
%if "%{_vendor}" == "redhat"
BuildRequires:  automake
BuildRequires:  libtool
BuildRequires:  vapoursynth-devel
%endif

%description
%{summary}.

%prep
%autosetup -c
cd %{_vpath_srcdir}
./autogen.sh
./configure --libdir=%{_libdir}/vapoursynth CPPFLAGS="%{optflags}"

%build
cd %{_vpath_srcdir}
make

%install
cd %{_vpath_srcdir}
%make_install

%files
%{_libdir}/vapoursynth/libhqdn3d.so
