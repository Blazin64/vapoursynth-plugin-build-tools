%global _vpath_srcdir vapoursynth-nnedi3-%{version}/

Name:           vapoursynth-nnedi3
Version:        12
Release:        1%{?dist}
Summary:        Fluxsmooth plugin for VapourSynth

License:        GPLv2
URL:            https://github.com/dubhater/vapoursynth-nnedi3/
Source:         %{url}archive/v%{version}/vapoursynth-nnedi3-%{version}.tar.gz

%if "%{_vendor}" == "debbuild"
%endif
%if "%{_vendor}" == "redhat"
BuildRequires:  automake
BuildRequires:  libtool
BuildRequires:  yasm
BuildRequires:  vapoursynth-devel
%endif

%description
%{summary}.

%prep
%autosetup
./autogen.sh
./configure --libdir=%{_libdir}/vapoursynth --datadir=%{_datadir} CPPFLAGS="%{optflags}"

%build
make

%install
%make_install

%files
%{_libdir}/vapoursynth/libnnedi3.so
%{_datadir}/nnedi3/nnedi3_weights.bin
