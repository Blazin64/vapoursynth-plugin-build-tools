%global _vpath_srcdir vapoursynth-sangnom-r%{version}/

Name:           vapoursynth-sangnom
Version:        42
Release:        1%{?dist}
Summary:        Sangnom plugin for VapourSynth

License:        MIT
URL:            https://github.com/dubhater/vapoursynth-sangnom/
Source:         %{url}archive/r%{version}/vapoursynth-sangnom-r%{version}.tar.gz

%if "%{_vendor}" == "debbuild"
%endif
%if "%{_vendor}" == "redhat"
BuildRequires:  automake
BuildRequires:  vapoursynth-devel
%endif

%description
%{summary}.

%prep
%autosetup -c

%build
%meson --libdir=%{_libdir}/vapoursynth
%meson_build

%install
%meson_install

%check
%meson_test

%files
%{_libdir}/vapoursynth/libsangnom.so
