%global _vpath_srcdir znedi3-r%{version}/

Name:           vapoursynth-znedi3
Version:        2
Release:        1%{?dist}
Summary:        ZNEDI3 plugin for VapourSynth

License:        GPLv2
URL:            https://github.com/sekrit-twc/znedi3/
Source:         %{url}archive/r%{version}/znedi3-r%{version}.tar.gz
Source1:        https://github.com/sekrit-twc/vsxx/archive/3a89adc8ebd7ac756c0cf72fb45375ecc418df86/vsxx-3a89adc8ebd7ac756c0cf72fb45375ecc418df86.tar.gz

%if "%{_vendor}" == "debbuild"
%endif
%if "%{_vendor}" == "redhat"
BuildRequires:  automake
BuildRequires:  vapoursynth-devel
%endif

%description
%{summary}.

%prep
%setup -c
rm -r %{_vpath_srcdir}vsxx
%setup -D -T -a 1 -n %{name}-%{version}/%{_vpath_srcdir}
mv vsxx-3a89adc8ebd7ac756c0cf72fb45375ecc418df86 vsxx

%build
cd %{_builddir}/%{name}-%{version}/%{_vpath_srcdir}
make X86=1 CPPFLAGS="-Iznedi3 -Ivsxx -Ivsxx/vapoursynth -fPIC %{optflags}"

%install
cd %{_builddir}/%{name}-%{version}/%{_vpath_srcdir}
mkdir -p %{buildroot}%{_libdir}/vapoursynth
mv vsznedi3.so nnedi3_weights.bin %{buildroot}%{_libdir}/vapoursynth/

%files
%{_libdir}/vapoursynth/vsznedi3.so
%{_libdir}/vapoursynth/nnedi3_weights.bin
